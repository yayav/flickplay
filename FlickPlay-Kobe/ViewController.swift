import UIKit
import SceneKit
import ARKit


class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var animations = [String: CAAnimation]()
    var idle:Bool = true
    
    var current_scene = 0
    var faceScanTime: Float = 0.0
    let cameraNode = SCNNode()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.showsStatistics = true
        //let scene = SCNScene()
        let scene = SCNScene(named: "art.scnassets/Kobe-1.scn")!
        sceneView.scene = scene
        
        /*
         
        let faceScanSpeed: Float = 0.08
        let faceScanScale: Float = 0.2
        
        faceScanTime = faceScanTime + faceScanSpeed
        let faceScanPosition = sin(faceScanTime)*faceScanScale
 
        let head = scene.rootNode.childNode(withName: "AnimatedText", recursively: true)
        if let uhead = head, let geometry = uhead.geometry {
            for material in geometry.materials {
                let offset = SCNMatrix4MakeTranslation(0, faceScanPosition, 0)
                material.diffuse.contentsTransform = offset
               // material.transparent.contentsTransform = offset
            }
        }
        */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
